import qualified RIO.Text                      as Text
import           RIO
import           Gitlab
import           System.Environment
import           Test.Hspec

data GitlabTestApp = GitlabTestApp {
  gitlabConfig :: GitlabConfig
, logFunc :: LogFunc
}

instance HasGitlabConfig GitlabTestApp where
  gitlabConfigL = lens gitlabConfig (\x y -> x { gitlabConfig = y })

instance HasLogFunc GitlabTestApp where
  logFuncL = lens logFunc (\x y -> x { logFunc = y })

runLogVanish :: (Show a) => GitlabTestApp -> RIO GitlabTestApp a -> IO ()
runLogVanish app f = void $ asIO $ runRIO app $ f >>= logInfo . displayShow

glproj = "locallycompact/gitlab-api"

main :: IO ()
main = do
  envPrivateToken <- liftIO $ getEnv "GITLAB_PRIVATE_TOKEN"
  let gconf = GitlabConfig
        { glBaseUrl = "https://gitlab.com"
        , glToken   = Text.pack envPrivateToken
        }

  logOptions <- logOptionsHandle stdout True

  withLogFunc logOptions $ \l -> do
    let app = GitlabTestApp gconf l
    hspec $ do
      describe "Gitlab.Projects"
        $ it "getCommitData works"
        $ runLogVanish app
        $ getCommitData glproj "master"
      describe "Gitlab.Wikis" $ do
        it "getProjectWiki works" $ runLogVanish app $ getProjectWiki glproj
        it "createWikiPage works"
          $ runLogVanish app
          $ createWikiPage glproj
          $ GitlabWikiPage "foo" "markdown" "foo" "foo"
